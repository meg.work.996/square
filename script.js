'use strict';

let isEditable; // хранит состояние чекбокса: true - если включен
const checkBox = document.querySelector('#checkBox'); // чекбокс
const textArea = document.querySelector('.textArea'); // текстареа
const saveBtn = document.querySelector('.saveBtn'); // кнопка сохранить

/**
 * меняет состояние флага isEditable
 */
checkBox.addEventListener('click', function (event) {
    isEditable = event.target.checked;
    textArea.toggleAttribute('disabled'); // тоглим аттрибуты disabled у текстареи...
    saveBtn.toggleAttribute('disabled'); // ...и у кнопки
});

const palettesCollection = document.querySelectorAll('.paletteItm'); // коллекция (почти массив) палетки цветов
const square = document.querySelector('.square'); // наш квадрат
/**
 * вешает обработчик события на каждый элемент коллекции
 */
for (let i = 0; i < palettesCollection.length; i++) {
    /**
     * присваивает цвет палетки фону квадрата
     */
    palettesCollection[i].addEventListener('click', function () {
        if (!isEditable) return;
        square.style.backgroundColor = window.getComputedStyle(event.target).backgroundColor;
    })
}

let direction; // направление трансформации
/**
 * отслеживает движение мышью над квадратом
 */
square.addEventListener('mousemove', function (event) {
    /**
     * проверяем, включен ли чекбокс
     */
    if (!isEditable) return;
    /**
     * меняем курсор в зависимости от положения курсора
     * event.offsetX - положение курсора во время генерации события относительно target
     */
    if (event.offsetX < 10) {
        event.target.style.cursor = 'ew-resize';
        direction = 'left';
    }
    else if (event.offsetY < 10) {
        event.target.style.cursor = 'ns-resize';
        direction = 'top';
    }
    // event.target.offsetWidth - ширина элемента, readonly
    else if (event.target.offsetWidth - event.offsetX < 10) {
        event.target.style.cursor = 'ew-resize';
        direction = 'right';
    }
    else if (event.target.offsetHeight - event.offsetY < 10) {
        event.target.style.cursor = 'ns-resize';
        direction = 'bottom';
    }
    else {
        event.target.style.cursor = 'move';
        direction = 'move';
    }
});

/**
 * Нажатие на квадрат
 */
square.onmousedown = function (event) {
    if (!isEditable) return;
    const defaultHeight = event.target.offsetHeight; // начальная высота
    const defaultWidth = event.target.offsetWidth; // начальная ширина
    const defaultY = event.pageY; // координата Y на момент нажатия относительно страницы
    const defaultX = event.pageX; // координата X на момент нажатия относительно страницы
    const innerX = event.offsetX; // внутренний отступ X на момент нажатия относительно элемента
    const innerY = event.offsetY; // внутренний отступ Y На момент нажатия относительно элемента
    const defaultLeftPos = parseInt(square.style.left, 10); // стиль left на момент события
    const defaultTopPos = parseInt(square.style.top, 10); // стиль top на момент события
    let difference; // расширение
    /**
     * если move то двигаем блок
     */
    if (direction === 'move') {
        document.onmousemove = function (event) {
            square.style.top = event.pageY - innerY + 'px'; // двигаем блок с учетом внутренних отступов
            square.style.left = event.pageX - innerX + 'px'; // чтобы блок не центрировался по курсору
            /**
             * сброс события
             */
            document.onmouseup = function (event) {
                document.onmousemove = null;
            }
        }
    }
    /**
     * если bottom, то расстягиваем вниз, далее по аналогии
     */
    else if (direction === 'bottom') {
        document.onmousemove = function (event) {
            difference = event.pageY - defaultY;
            square.style.height = defaultHeight + difference + 'px';
            document.onmouseup = function (event) {
                document.onmousemove = null;
            }
        }
    } else if (direction === 'right') {
        document.onmousemove = function (event) {
            difference = event.pageX - defaultX;
            square.style.width = defaultWidth + difference + 'px';
            document.onmouseup = function (event) {
                document.onmousemove = null;
            }
        }
    } else if (direction === 'left') {
        document.onmousemove = function (event) {
            difference = event.pageX - defaultX;
            square.style.left = defaultLeftPos + difference + 'px';
            square.style.width = defaultWidth - difference  + 'px';
        }
    } else if (direction === 'top') {
        document.onmousemove = function (event) {
            difference = event.pageY - defaultY;
            square.style.top = defaultTopPos + difference + 'px';
            square.style.height = defaultHeight - difference + 'px';
        }
    }
}

/**
 * выводит стили в текстареа
 */
saveBtn.addEventListener('click', function(event) {
    if (!isEditable) return;
    let resultsArray = [];
    resultsArray.push('position: ' + window.getComputedStyle(square).position);
    resultsArray.push('top: ' + window.getComputedStyle(square).top);
    resultsArray.push('left: ' + window.getComputedStyle(square).left);
    resultsArray.push('border: ' + window.getComputedStyle(square).border);
    resultsArray.push('background-color: ' + window.getComputedStyle(square).backgroundColor);
    resultsArray.push('width: ' + window.getComputedStyle(square).width);
    resultsArray.push('height: ' + window.getComputedStyle(square).height);

    let result = resultsArray.join('\n');
    textArea.textContent = result;
});